
package kz.alem.sendAboutNewLinks.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "setting")
public class SettingProperties {
    
   public String hostsetting; 

    public String getHostsetting() {
        return hostsetting;
    }

    public void setHostsetting(String hostsetting) {
        this.hostsetting = hostsetting;
    }

  
    public int portsetting;

    public int getPortsetting() {
        return portsetting;
    }

    public void setPortsetting(int portsetting) {
        this.portsetting = portsetting;
    }

   
    public String from;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
    
    public String  botApitoken;

    public String getBotApitoken() {
        return botApitoken;
    }

    public void setBotApitoken(String botApitoken) {
        this.botApitoken = botApitoken;
    }
    public String   groupChatId;

    public String getGroupChatId() {
        return groupChatId;
    }

    public void setGroupChatId(String groupChatId) {
        this.groupChatId = groupChatId;
    }
    
     public String urlString;

    public String getUrlString() {
        return urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }
   
}
