package kz.alem.sendAboutNewLinks.notification;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.model.MessagePartHeader;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.alem.sendAboutNewLinks.properties.SettingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SendMessage {

    @Autowired
    private SettingProperties properties;

    public static String content = "";
    private static final String APPLICATION_NAME = "Application";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String CREDENTIALS_FOLDER = "/credentials"; // Directory to store user credentials.

    /**
     * Global instance of the scopes required by this quickstart. If modifying
     * these scopes, delete your previously saved credentials/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_READONLY);
    private static final String CLIENT_SECRET_DIR = "/client_secret.json";

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If there is no client_secret.
     */
    public static List<String> extractUrls(String text) {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }

    public static String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    public static String getContent(Message message) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            getPlainTextFromMessageParts(message.getPayload().getParts(), stringBuilder);
            byte[] bodyBytes = Base64.decodeBase64(stringBuilder.toString());
            String text = new String(bodyBytes, "UTF-8");
            String outStr = "";
            List<String> extractedUrls = extractUrls(text);
            for (String url : extractedUrls) {

                url = url.replaceAll("https://www.google.com", "");
                List<String> extractedUrls2 = extractUrls(url);

                for (String url1 : extractedUrls2) {
                    outStr += getDomainName(url1) + "\r\n ";

                }

            }
            return outStr;
        } catch (Exception e) {
            return message.getSnippet();
        }
    }

    public static void getPlainTextFromMessageParts(List<MessagePart> messageParts, StringBuilder stringBuilder) {
        for (MessagePart messagePart : messageParts) {
            if (messagePart.getMimeType().equals("text/plain")) {
                stringBuilder.append(messagePart.getBody().getData());
            }

            if (messagePart.getParts() != null) {
                getPlainTextFromMessageParts(messagePart.getParts(), stringBuilder);
            }
        }
    }

    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {

        InputStream in = SendMessage.class.getResourceAsStream(CLIENT_SECRET_DIR);

        if (in == null) {
            throw new IOException("Client information not found.");
        }

        GoogleClientSecrets secrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow
                = new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, secrets, SCOPES)
                        .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(CREDENTIALS_FOLDER)))
                        .setAccessType("offline")
                        .build();
        flow.getCredentialDataStore().delete("user");

        LocalServerReceiver localReceiver = new LocalServerReceiver.Builder().setHost(properties.getHostsetting()).setPort(properties.getPortsetting()).build();
        Credential cred = new AuthorizationCodeInstalledApp(flow, localReceiver).authorize("user1");

        return cred;

    }

    public static List<Message> ListMessages(Gmail service, String userId,
            String query) throws IOException {
        ListMessagesResponse response = service.users().messages().list(userId).setQ(query).execute();

        List<Message> messages = new ArrayList<Message>();
        while (response.getMessages() != null) {
            messages.addAll(response.getMessages());
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = service.users().messages().list(userId).setQ(query)
                        .setPageToken(pageToken).execute();
            } else {
                break;
            }
        }

        for (Message message : messages) {
            System.out.println(message.toPrettyString());
        }

        return messages;
    }

    // public  void main(String... args) throws IOException, GeneralSecurityException {
    public void send() throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

        // Print the labels in the user's account.
        String user = "me";

        try {

            //  ListMessagesResponse mess = service.users().messages().list(user).execute();
            List<Message> message = ListMessages(service, user, "is:unread");
            //  mess.getMessages();

            //    Message m = service.users().messages().get("me", message.get(2).getId()).execute();
            // MessagePart part = message.get(0).getRaw()
            for (Message messageitem : message) {
                Message m = service.users().messages().get(user, messageitem.getId()).execute();
                List<MessagePartHeader> headers = m.getPayload().getHeaders();
                for (MessagePartHeader header : headers) {
                    if (header.getName().equals("From")) {
                        // if (header.getValue().equalsIgnoreCase("Google Alerts <googlealerts-noreply@google.com>")) {
                        if (header.getValue().equalsIgnoreCase(properties.getFrom())) {
                            content = getContent(m);
                            send(content);
                            System.out.println("result: " + content);
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());

        }
    }

    public void send(String message) {

        String apiToken = properties.getBotApitoken();  //"548095243:AAGgXG4OqrCOgBdbWAynGUnGF6SRhE4Enmc";

        try {
            String urlString = properties.getUrlString(); //"https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
            urlString = String.format(urlString, apiToken, properties.getGroupChatId(), URLEncoder.encode(message, "UTF-8")); //"-1001232715721"
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            StringBuilder sb = new StringBuilder();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            String response = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
