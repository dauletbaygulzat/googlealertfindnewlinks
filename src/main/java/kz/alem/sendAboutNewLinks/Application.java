package kz.alem.sendAboutNewLinks;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import kz.alem.sendAboutNewLinks.notification.SendMessage;
import kz.alem.sendAboutNewLinks.properties.SettingProperties;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger LOG = Logger.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
        } catch (Throwable t) {
            LOG.error(t.getMessage(), t);
            System.exit(1);
        }
    }
    @Autowired
  private SettingProperties  setting;
    @Autowired
  public SendMessage sendMessage =  new SendMessage();

    @Override
    public void run(String... strings) throws Exception {
        try {
          sendMessage.send();
          
        } catch (Throwable t) {
            LOG.error(t.getMessage(), t);
            System.exit(1);
        }
    }

}
